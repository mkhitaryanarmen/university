<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\Group;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faculties = Faculty::all();
        return view('university.students', compact('faculties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $this->validate($request, [
            'firstName' => 'required|max:255',
            'lastName' => 'required|max:255',
            'groupID' => 'required|max:11',
        ]);

        $students = new Student();
        $students->first_name = $request->firstName;
        $students->last_name = $request->lastName;
        $students->groups_id = $request->groupID;
        $students->save();
        return response(['msg' => [$request->firstName, $request->lastName, $request->groupID]]);
//        return response(['msg' => 'create successfully']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $groups = Group::all()->where("faculties_id", '=', $request->facultyID);
        return $groups;
    }

    /* GROUPS FOR EDIT */
    public function storeForEdit(Request $request)
    {

        $groups_for_edit = DB::table('groups')->select('id', 'group_name')->where('faculties_id', '=', $request->facultyID)->get();
        return $groups_for_edit;
    }



    /*FILTER FOR FACULTIES*/
    public function getSelectGroups(Request $request)
    {

        $groups = Group::all()->where("faculties_id", '=', $request->facultyID);
        return $groups;
    }



    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $first_name = $request->firstName;
        $last_name = $request->lastName;
        $faculty_name = $request->faculty_name;
        $group_name = $request->selectGroup;

        $result = DB::table('faculties')
            ->join('groups', 'faculties.id', '=', 'groups.faculties_id')
            ->join('students', 'groups.id', '=', 'students.groups_id')
            ->select('students.*', 'faculties.name', 'groups.group_name')
            ->where('students.first_name', 'LIKE', '%' . $first_name . '%')
            ->where('students.last_name', 'LIKE', '%' . $last_name . '%')
            ->orderBy('students.id');


        if (isset($faculty_name) && isset($group_name)) {
            $students = $result->where([
                ['faculties.name', '=', $faculty_name],
                ['groups.group_name', '=', $group_name],
            ])
                ->get();
        } elseif (isset($faculty_name)) {
            $students = $result->where('faculties.name', '=', $faculty_name)
                ->get();
        } else {
            $students = $result->get();
        }

        return view('university.readyByAjax', compact('students'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if ($request->ajax()) {
            $array = DB::table('students')
                ->select('students.first_name', 'students.last_name', 'groups.id as g_id', 'faculties.id as f_id')
                ->join('groups', 'students.groups_id', '=', 'groups.id')
                ->join('faculties', 'faculties.id', '=', 'groups.faculties_id')
                ->where('students.id', '=', $request->id)
                ->get();

            return response($array);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $this->validate($request, [
            'firstName' => 'required|max:255',
            'lastName' => 'required|max:255',
        ]);

        $first_name = $request->firstName;
        $last_name = $request->lastName;
        $s_groups_id = $request->groupID;
        $students_id = $request->studetns_id;


        $result = DB::table('students')
            ->join('groups', 'students.groups_id', '=', 'groups.id')
            ->where('students.id', '=', $students_id)
            ->update(['students.first_name' => $first_name, 'students.last_name' => $last_name, 'students.groups_id' => $s_groups_id]);


        return response(['msg' => 'updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->ajax()) {
            Student::destroy($request->id);
            return response(['msg' => 'delete successfully']);
        }
    }

}


//SELECT f.name, g.group_name, s.first_name, s.last_name, s.image
// FROM faculties AS f INNER JOIN groups AS g ON f.id = g.faculties_id
// INNER JOIN students AS s ON g.id = s.groups_id;


// UPDATE students AS s INNER JOIN groups AS g ON s.groups_id = g.id
// SET s.first_name = "ArmenAk", s.last_name = "Mkhitaran", s.groups_id = 8
//WHERE s.id = 26 AND s.groups_id = 6 AND g.faculties_id = 1;



