<?php

namespace App\Http\Controllers;

use App\Faculty;
use Illuminate\Http\Request;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faculties = Faculty::all();
        return view('university.faculty', compact('faculties'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'text' => 'required|max:255',
        ]);
        $faculty = new Faculty();
        $faculty->name = $request->text;
        $faculty->save();
        return response(['msg' => 'create successfully']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if ($request->ajax()) {
            return response(Faculty::find($request->id));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'text' => 'required|max:255',
        ]);
        if ($request->ajax()) {
            $faculty = Faculty::find($request->id);
            $faculty->name = $request->text;
            $faculty->save();
            return response(['msg' => 'update successfully']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->ajax()) {
            Faculty::destroy($request->id);
            return response(['msg' => 'delete successfully']);
        }
    }
}
