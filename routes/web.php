<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/* FACULTY CRUD HERE */

Route::get('faculty', 'FacultyController@index');

Route::post('faculty', 'FacultyController@store');

Route::post('edit', 'FacultyController@edit');

Route::post('update', 'FacultyController@update');

Route::post('delete', 'FacultyController@destroy');




/* STUDENTS CRUD HERE */


Route::get('students', 'StudentsController@index');

Route::get('students/readyByAjax', 'StudentsController@show');

Route::post('students/store', 'StudentsController@store');

Route::post('students/storeForEdit', 'StudentsController@storeForEdit');

Route::post('students/getSelectGroups', 'StudentsController@getSelectGroups');

Route::post('create', 'StudentsController@create');

Route::post('students/edit', 'StudentsController@edit');

Route::post('students/update', 'StudentsController@update');

Route::post('students/delete', 'StudentsController@destroy');

Route::get('students/search', 'StudentsController@search');






