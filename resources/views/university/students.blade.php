@extends('layouts.layout')

@section('content')

    <div class="container" style="margin-bottom: 250px">
        <h2>Bordered Table</h2>
        <p>The .table-bordered class adds borders to a table:</p>
        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" id="addNew">+ Add New
        </button>
        {{-- Trigger end --}}
        <br><br>
        <div></div>
        <div id="items">
            <table id="example" class="table  display table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>
                        <input type="search" placeholder="Serach First Name" id="search_first_name"> <br><br>
                        <input type="hidden" id="hidden_search_first_name">
                        First Name
                    </th>
                    <th>
                        <input type="search" placeholder="Serach Last Name" id="search_last_name"> <br><br>
                        <input type="hidden" id="hidden_search_last_name">
                        Last Name
                    </th>
                    <th>
                        <input type="hidden" id="hidden_filter_faculties">
                        <label for="">Search Faculties</label>
                        <select class="form-control" id="filter_faculties" title="">
                            <option value=""></option>
                            @foreach($faculties as $faculty)
                                <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                            @endforeach
                        </select>
                        Faculty
                    </th>
                    <th>
                        <label for="">Search Groups</label>
                        <input type="hidden" id="hidden_filter_groups">
                        <select class="form-control" id="filter_groups" title="" disabled>

                        </select>
                        Group
                    </th>
                    <th id="editSort">Edit</th>
                    <th id="deleteSort">Delete</th>
                </tr>
                </thead>
                {{--tBody HERE--}}
                <tbody>

                </tbody>
            </table>
        </div>
    </div>


    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id">
                    {{-- MODAL FORM START HERE --}}
                    <form action="" method="post" id="studentForm">
                        <div class="form-group">
                            <input type="text" name="firstName" id="firstName" title="" placeholder="First Name"
                                   class="form-control" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="lastName" id="lastName" title="" placeholder="Last Name"
                                   class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="">Faculties</label>
                            <select class="form-control" id="faculties" title="" required>
                                <option value=""></option>
                                @foreach($faculties as $faculty)
                                    <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" id="groupsDiv">
                            <label for="">Groups</label>
                            <input type="hidden" id="groupsForEdit">
                            <input type="hidden" id="groupsForEdit_chack">
                            <select class="form-control" id="groups" title="" required disabled>

                            </select>
                        </div>
                        <button type="submit" class="btn btn-info" id="add">ADD</button>
                        <button type="button" class="btn btn-info" id="save">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </form>
                    {{--END MODAL FORM--}}
                </div>
            </div>
        </div>
    </div>





    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            /* SEARCH FOR FIRST NAME */
            $('#search_first_name').on('keyup', function () {
                var search_first_name = $(this).val();
                $('#hidden_search_first_name').val(search_first_name);
                var search_last_name = $("#hidden_search_last_name").val();
                var faculty_name = $("#hidden_filter_faculties").val();
                var selectGroup = $("#hidden_filter_groups").val();
                readyByAjax(search_first_name, search_last_name, faculty_name, selectGroup);
            });


            /* SEARCH FOR LAST NAME */
            $('#search_last_name').on('keyup', function () {
                var search_last_name = $(this).val();
                $("#hidden_search_last_name").val(search_last_name);
                var search_first_name = $('#hidden_search_first_name').val();
                var faculty_name = $("#hidden_filter_faculties").val();
                var selectGroup = $("#hidden_filter_groups").val();
                readyByAjax(search_first_name, search_last_name, faculty_name, selectGroup);
            });

            /*FILTER FOR FACULTIES*/
            $("#filter_faculties").on('change', function (e) {
                var facultyID = e.target.value;
                $.ajax({
                    type: 'post',
                    url: "http://127.0.0.1:8000/students/getSelectGroups",
                    data: {
                        "facultyID": facultyID,
                        '_token': $("input[name=_token]").val()
                    },
                    success: function (data) {
                        $("#filter_groups").empty();
                        $("#filter_groups").append('<option></option>');
                        $.each(data, function (index, subCatObj) {
                            $("#filter_groups").append('<option>' + subCatObj.group_name + '</option>');
                        });
                    }
                });
                var search_first_name = $('#hidden_search_first_name').val();
                var selectFaculty = $(this).find("option:selected").text();
                var search_last_name = $("#hidden_search_last_name").val();
                $("#hidden_filter_faculties").val(selectFaculty);
                var selectGroup = $("#hidden_filter_groups").val();
                readyByAjax(search_first_name, search_last_name, selectFaculty, selectGroup);


                if($("#filter_groups").val() !== ""){
                    $("#filter_groups").prop("disabled", false);
                } else {
                    $("#filter_groups").prop("disabled", true);
                }


            });


            /*FILTER FOR GROUPS*/
            $("#filter_groups").on('change', function () {
                var selectGroup = $(this).find("option:selected").text();
                $("#hidden_filter_groups").val(selectGroup);
                var faculty_name = $("#hidden_filter_faculties").val();
                var search_first_name = $('#hidden_search_first_name').val();
                var search_last_name = $("#hidden_search_last_name").val();
                readyByAjax(search_first_name, search_last_name, faculty_name, selectGroup);
            });


            /* GET STUDENTS INTO TABLE  */
            readyByAjax();
            function readyByAjax(search_first_name, search_last_name, selectFaculty, selectGroup) {
                $.ajax({
                    type: 'get',
                    data: {
                        'firstName': search_first_name,
                        'lastName': search_last_name,
                        'faculty_name': selectFaculty,
                        'selectGroup': selectGroup
                    },
                    url: "http://127.0.0.1:8000/students/readyByAjax",
                    success: function (data) {
                        $("#example tbody").html(data);
                    }
                });
            }


            /* ADD NEW STUDENT */
            $("#studentForm").on("submit", function (e) {
                e.preventDefault();
                var groupID = $("#groups").val();
                var firstName = $("#firstName").val();
                var lastName = $("#lastName").val();
                if (groupID !== "" && firstName !== "" && lastName !== "") {
                    $.ajax({
                        type: 'post',
                        url: "http://127.0.0.1:8000/create",
                        data: {
                            'groupID': groupID,
                            'firstName': firstName,
                            'lastName': lastName,
                            '_token': $("input[name=_token]").val()
                        },
                        success: function () {
                            readyByAjax();
                        }
                    });
                }
                $('#myModal').modal('hide');
            });


            /* DROPDOWN GROUPS AJAX */
            $("#faculties").on("change", function (e) {
                var facultyID = e.target.value;

                if($("#groups").val() !== ""){
                    $("#groups").prop("disabled", false);
                } else {
                    $("#groups").prop("disabled", true);
                }

                $.ajax({
                    type: 'post',
                    url: "http://127.0.0.1:8000/students/store",
                    data: {
                        "facultyID": facultyID,
                        '_token': $("input[name=_token]").val()
                    },
                    success: function (data) {
                        $("#groups").empty();
                        $.each(data, function (index, subCatObj) {
                            $("#groups").append('<option value="' + subCatObj.id + '">' + subCatObj.group_name + '</option>');
                        });
                    }
                });

            });


            /* GET STUDENTS FOR EDIT */
            $(document).on("click", ".edit", function () {

                $("#save").css("display", "inline");
                $("#add").css("display", "none");
                var id = $(this).val();
                $("#id").val(id);

                $.ajax({
                    type: 'post',
                    url: "http://127.0.0.1:8000/students/edit",
                    data: {'id': id, '_token': $("input[name=_token]").val()},
                    success: function (data) {
                        $.each(data, function (index, subCatObj) {
                            $("#firstName").val(subCatObj.first_name);
                            $("#lastName").val(subCatObj.last_name);
                            $("#faculties").val(subCatObj.f_id);
                            $("#groupsForEdit_chack").val(subCatObj.g_id);
                            $("#groupsForEdit").val(subCatObj.f_id);
                            editGroups();
                        });
                    }
                });


                /* EDIT FOR GROUPS */
                function editGroups() {
                    var facultyID = $("#groupsForEdit").val();
                    var groups_ID =  $("#groupsForEdit_chack").val();
                    $.ajax({
                        type: 'post',
                        url: "http://127.0.0.1:8000/students/storeForEdit",
                        data: {
                            "facultyID": facultyID,
                            '_token': $("input[name=_token]").val()
                        },
                        success: function (data) {
                            $("#groups").empty();
                            $.each(data, function (index, subCatObj) {
                                $("#groups").append('<option value="' + subCatObj.id + '">' + subCatObj.group_name + '</option>');
                                $("#groups").val(groups_ID);
                            });
                        }
                    });
                }


            });


            /* UPDATE STUDENT HERE  */
            $("#save").on("click", function () {
                var firstName = $("#firstName").val();
                var lastName = $("#lastName").val();
                var groupID = $("#groups").val();
                var studentsID = $("#id").val();
                if (firstName !== "" && lastName !== "" && groupID !== "" && studentsID !== "") {
                    $.ajax({
                        type: 'post',
                        url: "http://127.0.0.1:8000/students/update",
                        data: {
                            'studetns_id': studentsID,
                            'firstName': firstName,
                            'lastName': lastName,
                            'groupID': groupID,
                            '_token': $("input[name=_token]").val()
                        },
                        success: function (data) {
                            console.log(data);
                            readyByAjax();
                        }
                    });
                    $('#myModal').modal('hide');
                }
            });


            /*DELETE STUDENT*/
            $(document).on('click', '.deleteStudent', function () {
                var id = $(this).val();
                $.ajax({
                    type: 'post',
                    url: "http://127.0.0.1:8000/students/delete",
                    data: {'id': id},
                    dataType: 'json',
                    success: function () {
                        readyByAjax();
                    }
                });
            });


            /* CLICK ADDNEW HERE FOR display button ADD */
            $("#addNew").click(function () {
                $("#save").css("display", "none");
                $("#add").css("display", "inline");
                $("#faculties").val("");
                $("#groups").val("");
                $("#firstName").val("");
                $("#lastName").val("");
            });


        });








    </script>



@endsection