@foreach($students as $student)
    <tr role="row">
        <td class="sorting_1">{{$student->first_name}}</td>
        <td>{{$student->last_name}}</td>
        <td>{{$student->name}}</td>
        <td>{{$student->group_name}}</td>
        <td>
            <button class="btn btn-primary edit" value="{{$student->id}}" data-toggle="modal"
                    data-target="#myModal">Edit
            </button>
        </td>
        <td>
            <button class="btn btn-danger deleteStudent" value="{{$student->id}}">Delete</button>
        </td>
    </tr>
@endforeach