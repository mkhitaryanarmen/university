@extends('layouts.layout')

@section('content')

    <div class="container">
        <h2>Bordered Table</h2>
        <p>The .table-bordered class adds borders to a table:</p>
        <div class="col-md-4">
            <form {{--action="http://127.0.0.1:8000/faculty/store"--}} method="post" id="form">
                {{--{{ csrf_field() }}--}}
                <input type="text" class="form-control-static" title="" id="inputFaculty" required style="padding-top: 3px; padding-bottom: 5px">
                <button class="btn btn-primary" id="addButton">ADD</button>
            </form>
            <br><br>
        </div>

        <div id="items" style="margin-bottom: 250px">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Faculty Name</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                @foreach($faculties as $faculty)
                    <tbody>
                    <tr>
                        <td>{{$faculty->name}}</td>
                        <td>
                            <button class="btn btn-primary" value="{{$faculty->id}}" data-toggle="modal"
                                    data-target="#myModal" id="edit">Edit
                            </button>
                        </td>
                        <td>
                            <button class="btn btn-danger" id="deleteFaculty" value="{{$faculty->id}}">Delete</button>
                        </td>
                    </tr>
                    </tbody>
                @endforeach
            </table>
        </div>

    </div>









    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 330px; margin: auto">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body text-center">
                    <input type="hidden" id="id">
                    {{-- MODAL FORM START HERE --}}
                    <form method="post" id="formUpdate">
                        <input type="text" class="form-control-static" title="" id="editFaculty" style="padding-top: 3px; padding-bottom: 5px" required>
                        <button class="btn btn-primary " id="editButton">EDIT</button>
                    </form>
                    {{-- MODAL FORM END HERE --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>












    <script>

        /* ADD NEW FACULTY */
        $('#form').on('submit', function (e) {
            e.preventDefault();

            var text = $("#inputFaculty").val();
            if (text !== "") {
                $.ajax({
                    type: 'post',
                    url: "http://127.0.0.1:8000/faculty",
                    data: {'text': text, '_token': $("input[name=_token]").val()},
                    success: function (data) {
                        $('#items').load(location.href + ' #items');   // refresh
                        console.log(data.msg);
                    }
                });
            }
            $("#inputFaculty").val("");
        });


        /* GET FACULTY FOR EDIT*/
        $(document).on("click", "#edit", function () {
            var id = $(this).val();
            $("#id").val(id);
            $.ajax({
                type: 'post',
                url: "http://127.0.0.1:8000/edit",
                data: {'id': id, '_token': $("input[name=_token]").val()},
                success: function (data) {
                    $('#items').load(location.href + ' #items');   // refresh
                    var frmupdaqte = $("#formUpdate");
                    frmupdaqte.find("#editFaculty").val(data.name);
                }
            });
        });

        /* UPDATE FACULTY */
        $('#formUpdate').on('submit', function (e) {
            e.preventDefault();
            var text = $("#editFaculty").val();
            var id = $("#id").val();
            $.ajax({
                type: 'post',
                url: "http://127.0.0.1:8000/update",
                data: {'text': text, 'id': id, '_token': $("input[name=_token]").val()},
                success: function (data) {
                    $('#items').load(location.href + ' #items');   // refresh
                    console.log(data.msg);
                }
            });
            $('#myModal').modal('hide');
        });


        /*DELETE FACULTY*/
        $(document).on('click', '#deleteFaculty', function () {
            var id = $(this).val();
            $.ajax({
                type: 'post',
                url: "http://127.0.0.1:8000/delete",
                data: {'id': id},
                dataType: 'json',
                success: function (data) {
                    $('#items').load(location.href + ' #items');   // refresh
                    console.log(data.msg);
                }
            })
        });

    </script>



@endsection


