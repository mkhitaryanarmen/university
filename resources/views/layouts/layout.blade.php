<?php
/**
 * Created by PhpStorm.
 * User: PC5
 * Date: 11.07.2017
 * Time: 14:29
 */
?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>University</title>
    {{-- BOOTSTRAP MIN CSS --}}
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    {{-- JQUERY DATATABLE MIN CSS --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>

    {{-- JQUERY MIN JS --}}
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

    {{-- BOOTSTRAP MIN JS --}}
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>


<div class="jumbotron">
    <div class="container">
        <h1>Hello, World</h1>
        <p>This is our new blog</p>
        <a href="http://127.0.0.1:8000/students">Students</a>
        <a href="http://127.0.0.1:8000/faculty">Faculty</a>
    </div>
</div>

<div class="container">
    @yield('content')
</div>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

</body>
</html>

