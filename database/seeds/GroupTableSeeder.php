<?php

use Illuminate\Database\Seeder;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('groups')->insert(
            array(
                [
                    "group_name"=>"First Group",
                    "factory_id"=>"1"
                ],
                [
                    "group_name"=>"Second Group",
                    "factory_id"=>"1"
                ],
                [
                    "group_name"=>"First Group",
                    "factory_id"=>"2"
                ],
                [
                    "group_name"=>"Second Group",
                    "factory_id"=>"2"
                ],
                [
                    "group_name"=>"First Group",
                    "factory_id"=>"3"
                ],
                [
                    "group_name"=>"Second Group",
                    "factory_id"=>"3"
                ],
                [
                    "group_name"=>"First Group",
                    "factory_id"=>"4"
                ],
                [
                    "group_name"=>"Second Group",
                    "factory_id"=>"4"
                ]
            )
        );
    }
}
