<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(FactoryTableSeeder::class);
         $this->call(GroupTableSeeder::class);
         $this->call(StudentsTableSeeder::class);
    }
}
