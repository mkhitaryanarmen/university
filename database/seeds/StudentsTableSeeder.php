<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Illuminate\Support\Facades\DB::table('students')->insert(
            array(
                [
                    "first_name" => "Armen",
                    "last_name" => "Mkhitaryan",
                    "image" => "",
                    "groups_id" => "2"
                ],
                [
                    "first_name" => "Argishti",
                    "last_name" => "Yeghiazaryan",
                    "image" => "",
                    "groups_id" => "1"
                ],
                [
                    "first_name" => "Arshak",
                    "last_name" => "Aghabekyan",
                    "image" => "",
                    "groups_id" => "1"
                ],
                [
                    "first_name" => "Nshan",
                    "last_name" => "Ustyan",
                    "image" => "",
                    "groups_id" => "2"
                ]
            )
        );
    }
}
