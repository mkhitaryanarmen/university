<?php

use Illuminate\Database\Seeder;

class FactoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('faculties')->insert(
            array(
                [
                    'name'=>"Programing",
                ],
                [

                    "name"=>"Economics"
                ],
                [
                    "name"=>"Journalism"
                ],
                [
                    "name"=>"Gis Engineer"
                ]
            )
        );
    }
}
